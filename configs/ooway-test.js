module.exports = {
    name: 'Oogway Test Server',
    id: '853868805473173534',
    welcome_channel_id: '867403009226571846',
    new_member_default_role_id: '854359546738769980',
    core_team_role_id: '867426995918340109',
    log_channel_id: '869303637153824768',
    showcase_category_id: '881959818347294721',
    showcase_channel_id: '881960661268201473',
};
